<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * Define type of primary key.
     * 
     * @var string
     */
    protected $keyType = 'string';
}
