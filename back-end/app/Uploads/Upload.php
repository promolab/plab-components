<?php

namespace App\Uploads;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    /**
     * Define type of primary key.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'id', 'parent_id', 'user_upload_id', 'category', 'title', 'original_title', 'extension', 'size'
    ];

    /**
     * Define if id is incremental.
     * 
     * @var boolean
     */
    public $incrementing = false;
}
