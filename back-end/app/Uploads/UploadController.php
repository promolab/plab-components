<?php

namespace App\Uploads;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Auth;
use Storage;

/**
 * Grazie a questo controller sarà possibile effettuare degli uploads di file sul server.
 * L'upload verrà fatto sulla base del file che si vuole caricare, la logica è validare il tipo di file e se 
 * validazione avvenuta con successo effettuare l'upload a filesystem.
 */
class UploadController extends Controller
{
    /**
     * Request 
     * 
     * @var Request
     */
    protected $request;

    /**
     * Istanzio richiesta a livello di classe
     * 
     * @var Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Salvo le modifiche sull'upload
     * 
     * @return Response
     */
    public function save()
    {
        $row = Upload::findOrFail($this->request->id);
        $row->title = $this->request->title;
        $row->save();

        return response()->json(['message' => 'success']);
    }

    /**
     * Eliminto l'upload
     * 
     * @return Response
     */
    public function delete($id)
    {
        $row = Upload::findOrFail($id);
        $row->delete();

        return response()->json(['message' => 'success']);
    }

    /**
     * Effettuo il download del file
     * 
     * @return Response
     */
    public function download()
    {
        $row = Upload::findOrFail($this->request->id);

        $folder = UploadHelper::getFullPathDestination($row->category, $row->parent_id);
        $path = "{$folder}/{$row->id}.{$row->extension}";

        return response()->download($path, $row->title);
    }

    /**
     * Dopo aver effettuato la validazione del file, effettuo l'upload vero e proprio del file su filesystem.
     * 
     * @return bool 
     */
    private function upload()
    {
        // Estraggo variabili necessarie
        // $user = Auth::user(); // TODO: Decommentare in produzione
        $file = Input::file('file');
        $extension = $file->getClientOriginalExtension();
        $title = basename( "{$file->getClientOriginalName()}" );
        $params = (object) json_decode($this->request->params);
        
        // Salvo a database la riga dell'upload
        $row = Upload::create([
            'id'                => (string) Str::uuid(),
            'parent_id'         => $params->parentId,
            'user_upload_id'    => '0d4a2a46-a944-11e9-a099-ccbd515ad006',
            'category'          => $params->category,
            'title'             => $title,
            'original_title'    => $title,
            'extension'         => $extension,
            'size'              => $file->getSize()
        ]);

        // Salvo a filesystem il file appena caricato
        $folderDestination = UploadHelper::getFolderDestination($params->category, $params->parentId);
        $nameFile = "{$row->id}.{$extension}";
        Storage::disk('uploads')->putFileAs($folderDestination, $file, $nameFile);

        return new UploadResource( $row );
    }

    /**
     * Valido l'upload di tutte le immagini.
     * 
     * @return Response
     */
    public function image()
    {
        $this->validate($this->request,[
            'file' => 'image|max:5000'
        ]);

        return $this->upload();
    }
}
