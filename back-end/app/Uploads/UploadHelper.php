<?php

namespace App\Uploads;

use Storage;

class UploadHelper
{
    /**
     * Ricavo la path relativa della cartella dove salvare il file
     * 
     * @param string    $category
     * @param string    $parentId
     * 
     * @return string
     */
    public static function getFolderDestination($category, $parentId)
    {
        return ( is_null( $parentId ) ) ? "{$category}" : "{$parentId}/{$category}";
    }

    /**
     * Ricavo la path assoluta della cartella dove salvare il file
     * 
     * @param string    $category
     * @param string    $parentId
     * 
     * @return string
     */
    public static function getFullPathDestination($category, $parentId)
    {
        $path = self::getFolderDestination($category, $parentId);
        return Storage::disk('uploads')->path($path);
    }

    /**
     * Ritorno il link del file
     * 
     * @param mixed
     * @return string
     */
    public static function getLinkFile($upload)
    {
        $folder = self::getFolderDestination($upload->category, $upload->parent_id);
        $path = "{$folder}/{$upload->id}.{$upload->extension}";

        return Storage::disk('uploads')->url($path);
    }
}
