<?php

namespace App\Uploads;

use Illuminate\Http\Resources\Json\JsonResource;

class UploadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'user_upload_id' => $this->user_upload_id,
            'category' => $this->category,
            'title' => $this->title,
            'original_title' => $this->original_title,
            'extension' => $this->extension,
            'size' => $this->size,
            'link' => UploadHelper::getLinkFile($this)
        ];
    }
}
