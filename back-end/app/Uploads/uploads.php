<?php

Route::post('uploads', 'UploadController@save');

Route::delete('uploads/{id}', 'UploadController@delete');

Route::post('downloads', 'UploadController@download');

Route::post('uploads/image', 'UploadController@image');
