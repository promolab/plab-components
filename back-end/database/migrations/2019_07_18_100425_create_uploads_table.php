<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->uuid('id')->primary('id');
            $table->uuid('parent_id')->index()->nullable(); // Id di un eventuale genitore dell'upload
            $table->uuid('user_upload_id')->index(); // Id di chi effettua l'upload
            $table->string('category', 100)->nullable(); // Stringa che riconosce la macro categoria dell'upload
            $table->string('title', 200)->nullable();
            $table->string('original_title', 200)->nullable();
            $table->string('extension', 10)->nullable();
            $table->integer('size');
            $table->timestamps();

            $table->foreign('user_upload_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploads');
    }
}
