<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\Article;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        for ($i=0; $i<10000; $i++) {
            $article = new Article;
            $article->id = $faker->uuid;
            $article->name = $faker->word;
            $article->category = $faker->word;
            $article->save();
        }
    }
}
