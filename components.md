# Componenti

Questo file elenca tutti i componenti, ha lo scopo di tenere traccia di tutti i componenti presenti in questa repo. <br>
Per componente si intende o un componente classico front-end o un insieme limitato di classi per quando riguarda il bac-kend.

## Virtual Scroller

Componente che permette di effettuare il virtual scroll di una lista, in particolare questa lista può essere caricata sincrona o asincrona.

**Path**: `front-end/src/app/components/virtual-scroll`

![scheme](assets/virtual-scroll.jpg)

## Drag & Drop Sorting

Componente che permette di ordinare un lista tramite drag & drop.

**Path**: `front-end/src/app/components/drag-drop-sorting`
