ISTRUZIONI CONFIGURAZIONE FRONT END

- npm install
- test su http://localhost:5000

EXAMPLE OF .htaccess FOR MOD_REWRITE

```

<IfModule mod_rewrite.c>
  RewriteEngine On
  RewriteBase /
  RewriteRule ^index\.html$ - [L]
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule . /index.html [L]
</IfModule>

```