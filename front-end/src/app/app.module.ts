import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* Material */
import { MaterialModule } from './material.module';
import { MAT_DATE_LOCALE } from '@angular/material/core';

/* Default Component */
import { AppComponent } from './app.component';

/* Authentication */
import { ErrorService } from './plab/error/error.service';

/* Personal Services */
import { CommonService } from './shared/services/common.service';
import { GlobalService } from './shared/services/global.service';
import { LocaleService } from 'angular-l10n';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { routing } from './app.routing';

import { ScrollingModule } from '@angular/cdk/scrolling';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FileUploadModule } from 'ng2-file-upload';

import { VirtualScrollComponent } from './components/virtual-scroll';
import { DragDropSortingComponent } from './components/drag-drop-sorting';
import { WrapperUploadsFilesComponent, MultipleUploadsComponent } from './components/wrapper-uploads-files';
import { SingleUploadsComponent } from './components/wrapper-uploads-files';
import { ShowUploadsComponent } from './components/wrapper-uploads-files';

import { FileSizePipe } from './components/wrapper-uploads-files';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    ScrollingModule,
    DragDropModule,
    FileUploadModule,
    routing
  ],
  declarations: [
    AppComponent,
    VirtualScrollComponent,
    DragDropSortingComponent,
    WrapperUploadsFilesComponent,
    SingleUploadsComponent,
    ShowUploadsComponent,
    MultipleUploadsComponent,
    FileSizePipe
  ],
  entryComponents: [
  ],
  providers: [
    ErrorService,
    CommonService,
    GlobalService,
    LocaleService,
    { provide: 'Window', useValue: window },
    { provide: MAT_DATE_LOCALE, useValue: 'it-IT' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
