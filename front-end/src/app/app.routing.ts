import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VirtualScrollComponent } from './components/virtual-scroll/virtual-scroll.component';
import { DragDropSortingComponent } from './components/drag-drop-sorting';
import { WrapperUploadsFilesComponent } from './components/wrapper-uploads-files';

const appRoutes: Routes = [
  
  { path: 'virtual-scroll', component: VirtualScrollComponent },
  { path: 'drag-drop-sorting', component: DragDropSortingComponent },
  { path: 'uploads-files', component: WrapperUploadsFilesComponent },
  { path: '', redirectTo: '/', pathMatch: 'full' },

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, {useHash: true});