export class ArticleModel {

    public id: string;
    public name: string;
    public category: string;
    
    constructor(obj) {
      this.id = obj.id;
      this.name = obj.name;
      this.category = obj.category;
    }
  }
  