export class PaginatorModel {

  public total: number;
  public currentPage: number;
  public lastPage: number;
  public firstPageUrl: string;
  public lastPageUrl: string;
  public nextPageUrl: string;
  public prevPageUrl: string;
  public data: Array<any>;
  public isLoadingMore: boolean;
  
  constructor(obj?) {
    if (obj) {      
      this.total = obj.total;
      this.currentPage = obj.current_page;
      this.lastPage = obj.last_page;
      this.firstPageUrl = obj.first_page_url;
      this.lastPageUrl = obj.last_page_url;
      this.nextPageUrl = obj.next_page_url;
      this.prevPageUrl = obj.prev_page_url;
      this.data = obj.data;
    } else {
      this.isLoadingMore = false;
    }
  }
}
