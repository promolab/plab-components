import { Component, ViewChild, OnInit } from '@angular/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { VirtualScrollService } from './virtual-scroll.service';
import { PaginatorModel } from './paginator.model';
import { ArticleModel } from './article.model';

@Component({
  selector: 'virtual-scroll',
  templateUrl: './virtual-scroll.html',
  styleUrls: ['./virtual-scroll.scss'],
  providers: [
    VirtualScrollService
  ]
})
export class VirtualScrollComponent implements OnInit {

  @ViewChild(CdkVirtualScrollViewport) viewport: CdkVirtualScrollViewport;

  public articles: Array<ArticleModel> = [];
  public paginator: PaginatorModel = new PaginatorModel;

  constructor(
    private virtualScrollService: VirtualScrollService
  ) { }

  ngOnInit() {
    this.getData();
  }

  /**
   * Evento scatenato dallo scroll del VirtualScroller
   * @return void
   */
  public nextBatch = () => {
    if (!this.paginator.currentPage || this.paginator.currentPage === this.paginator.lastPage)
      return;

    const end = this.viewport.getRenderedRange().end;
    const total = this.viewport.getDataLength();
    if (end > (total - 5))
      this.getData(this.paginator.nextPageUrl);
  }

  /**
   * Ricavo la prossima pagina di elementi e li aggiungo all'array di elementi
   * @return void
   */
  private getData = (url?: string) => {
    if (this.paginator.isLoadingMore) return;

    this.paginator.isLoadingMore = true;
    this.virtualScrollService.get(url).subscribe(
      (results: PaginatorModel) => {
        this.paginator = results;
        this.articles = [...this.articles, ...results.data];
        this.paginator.isLoadingMore = false;
      }
    );
  }
}
