import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { map, catchError, take } from 'rxjs/operators';
import { PaginatorModel } from './paginator.model';
import { ArticleModel } from './article.model';

@Injectable()
export class VirtualScrollService {
  private url: string = '';

  constructor(private http: HttpClient) {
    this.url = process.env.API_URL + 'articles';
  }

  /**
   * Ricavo gli elementi da back-end
   * @param   url
   * @return  PaginatorModel
   */
  public get = (url: string = this.url): Observable<PaginatorModel> =>
    this.http.get(url).pipe(
      map((body: any) => {
        let paginator = new PaginatorModel(body);
        paginator.data = body.data.map((row: object) => new ArticleModel(row));
        return paginator;
      }),
      catchError(this.handleError)
    );

  private handleError = (error: Response | any) => throwError(error);
}
