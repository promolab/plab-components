import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { UploadModel } from '..';

const CONF = {
  url: '',
  method: 'POST',
  authToken: '',
  headers: [{
    name: 'Accept',
    value: 'application/json'
  }]
};

@Component({
  selector: 'app-multiple-uploads',
  templateUrl: './multiple-uploads.component.html',
  styleUrls: ['./multiple-uploads.component.scss']
})
export class MultipleUploadsComponent implements OnInit {

  @Input() dropZoneSelect = false;
  @Input() showToast = true;
  @Input() type: string;
  @Input() parentId: string = null;
  @Input() category: string;

  @Output() successUploadEmitter: EventEmitter<void> = new EventEmitter();
  @Output() errorUploadEmitter: EventEmitter<void> = new EventEmitter();

  public uploader: FileUploader;
  public hasDropZoneHover = false;
  public attachments: FileItem[] = [];

  constructor(
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.setFileUploader();
    this.registerEventOnObject();
  }

  /**
   * Inizializzo componente uploader
   *
   * @return void
   */
  private setFileUploader = () => {
    CONF.url = `${process.env.BACKEND_URL}uploads/${this.type}`;
    // CONF.authToken = `Bearer ${this.loginService.accessToken}`;

    this.uploader = new FileUploader(CONF);
  }

  /**
   * Funzione richiamata all'hover della dropZone
   *
   * @return void
   */
  public fileHover = (e: any) => {
    this.hasDropZoneHover = e;
  }

  /**
   * Registro gli eventi sull'uploader
   *
   * @return void
   */
  private registerEventOnObject = () => {
    this.onBuildItemForm();
    this.onAfterAddingFile();
    this.setErrorItem();
    this.setSuccessItem();
  }

  /**
   * Inserisco paramentri opzionali per sapere di che documento si tratta
   *
   * @return void
   */
  private onBuildItemForm = () => {
    this.uploader.onBuildItemForm = (item, form) => {
      const obj = { parentId: this.parentId, category: this.category };
      form.append('params', JSON.stringify(obj));
    };
  }

  /**
   * Evento dopo l'aggiunta del file per l'upload, a differenza degli upload multipli una volta selezionato il file l'utente
   * non ha altre operazioni da fare, quindi inutile fargli cliccare un altra volta.
   * Registro l'evento dopo l'aggiunta di ogni file setto il parametro a false, setto
   * il paramentro a false se no riporta il seguente errore:
   *
   *  Failed to load http://localhost:8000/api/upload: Response to preflight request doesn't pass access control check:
   *  The value of the 'Access-Control-Allow-Origin' header in the response must not be the wildcard '*' when
   *  the request's credentials mode is 'include'. Origin 'http://localhost:5000' is therefore not allowed access.
   *  The credentials mode of requests initiated by the XMLHttpRequest is controlled by the withCredentials attribute.
   *
   * @return void
   */
  private onAfterAddingFile = () => {
    this.uploader.onAfterAddingFile = (item => {
      this.attachments.push(item);
      item.withCredentials = false;
    });
  }

  /**
   * Operazioni da eseguire nel caso un item vada in errore
   *
   * @return void
   */
  private setErrorItem = () => {
    this.uploader.onErrorItem = (item, response: string, status: number, headers) => {
      const error = JSON.parse(response);
      // tslint:disable-next-line:no-string-literal
      item['errors'] = (error.errors && error.errors.file) ? `${error.message} ${error.errors.file}` : `${error.message}`;
      this.errorUploadEmitter.emit();
    };
  }

  /**
   * Operazioni da eseguire nel caso un allegato sia caricato correttamente. Avviso il genitore che l'upload è avvenuto con successo,
   * rimuovo il file dal componente upload e mostro il toast solo se definito dal componente
   *
   * @return void
   */
  private setSuccessItem = () => {
    this.uploader.onSuccessItem = (item, response: string, status: number, headers) => {
      const res = JSON.parse(response);
      const upload = new UploadModel(res.data);
      item.remove();

      // Nel caso sia ultimo upload emetto l'evento di success
      if (this.uploader.queue.length === 0) {
        this.successUploadEmitter.emit();
      }

      if (this.showToast) {
        this.snackBar.open('Upload effettuato con successo', null, {
          duration: 4000
        });
      }
    };
  }

  /**
   * Cancello e rimuovo tutti gli allegati dalla coda
   *
   * @return void
   */
  public resetQueue = () => {
    this.uploader.cancelAll();
    this.uploader.clearQueue();
  }

  get queueElements() {
    return this.uploader.queue.length;
  }
}
