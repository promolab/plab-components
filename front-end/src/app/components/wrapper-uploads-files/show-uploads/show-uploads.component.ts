import { Component, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { UploadService } from '../upload.service';
import { UploadModel } from '..';

@Component({
  selector: 'app-show-uploads',
  templateUrl: './show-uploads.html',
  styleUrls: ['./show-uploads.scss'],
  providers: [
    UploadService
  ]
})
export class ShowUploadsComponent {

  @Input() uploads: UploadModel[] = [];

  constructor(
    private snackBar: MatSnackBar,
    private uploadService: UploadService
  ) { }

  /**
   * Salvo le modifiche sull'upload
   *
   * @param UploadModel upload
   * @return void
   */
  public save = (upload: UploadModel) => {
    this.uploadService.saveUpload(upload).subscribe((results: boolean) => {
      upload.inEditing = false;
      this.snackBar.open('Allegato salvato con successo', '', {
        duration: 4000,
      });
    });
  }

  /**
   * Effettuo il download del file
   *
   * @param UploadModel upload
   * @return void
   */
  public download = (upload: UploadModel) => {
    this.uploadService.downloadFile(upload).subscribe();
  }

  /**
   * Elimino l'upload definito
   *
   * @param UploadModel upload
   * @return void
   */
  public delete = (upload: UploadModel) => {
    this.uploadService.deleteUpload(upload.id).subscribe((results: boolean) => {
      const index = this.uploads.map(o => o.id).indexOf(upload.id);
      this.uploads.splice(index, 1);
      this.snackBar.open('Allegato eliminato con successo', '', {
        duration: 4000,
      });
    });
  }
}
