export class UploadModel {
    public id: string;
    public parentId: string;
    public userUploadId: string;
    public category: string;
    public title: string;
    public originalTitle: string;
    public extension: string;
    public size: number;
    public link: string;
    public inEditing: boolean;
    
    constructor(obj) {
        this.id = obj.id;
        this.parentId = obj.parent_id;
        this.userUploadId = obj.user_upload_id;
        this.category = obj.category;
        this.title = obj.title;
        this.originalTitle = obj.original_title;
        this.extension = obj.extension;
        this.size = obj.size;
        this.link = obj.link;
        this.inEditing = false;
    }

    /**
     * Toggl su prorietà `inEditing`
     * 
     * @return void
     */
    public toggleEdit = () => {
        this.inEditing = !this.inEditing;
    }
}
