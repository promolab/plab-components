import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { first, map, catchError } from 'rxjs/operators';
import { UploadModel } from '.';

@Injectable()
export class UploadService {

  private urlUpload: string;
  private urlDownload: string;

  constructor(private http: HttpClient) {
    this.urlUpload = `${process.env.BACKEND_URL}uploads`;
    this.urlDownload = `${process.env.BACKEND_URL}downloads`;
  }

  /**
   * Salvo le modifiche sull'upload
   *
   * @param upload UploadModel
   * @return boolean
   */
  public saveUpload = (upload: UploadModel): Observable<boolean> =>
    this.http.post(this.urlUpload, upload).pipe(
      first(),
      map((res: any) => true),
      catchError(this.handleError)
    )

  /**
   * Effettuo il download del file
   *
   * @param upload UploadModel
   * @return boolean
   */
  public downloadFile = (upload: UploadModel): Observable<void> =>
    this.http.post(`${this.urlDownload}`, upload, { responseType: 'blob' }).pipe(
      map((res: any) => {
        let url = window.URL.createObjectURL(res);
        let a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = upload.title;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }),
      catchError(this.handleError)
    )

  /**
   * Elimino l'upload
   *
   * @param uploadId string
   * @return boolean
   */
  public deleteUpload = (uploadId: string): Observable<boolean> =>
    this.http.delete(`${this.urlUpload}/${uploadId}`).pipe(
      first(),
      map((res: any) => true),
      catchError(this.handleError)
    )

  private handleError = (error: Response | any) => Observable.throw(error);
}
