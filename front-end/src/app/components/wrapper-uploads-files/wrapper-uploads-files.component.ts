import { Component } from '@angular/core';

@Component({
  selector: 'wrapper-uploads-files',
  templateUrl: './wrapper-uploads-files.html',
  styleUrls: ['./wrapper-uploads-files.scss'],
  providers: [
  ]
})
export class WrapperUploadsFilesComponent {

  public files = [];

  constructor(
  ) { }
}
