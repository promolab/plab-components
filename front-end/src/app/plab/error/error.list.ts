import { PlabErrorModel } from './error.model'

export class ErrorList {

    public static GetErrorMessage = (objError) => {
        // preparo il messaggio di risposta con un errore generico
        let retval = new PlabErrorModel();

        // trovo nella lista l'errore in base a httpCode
        let mainErrFind = ErrorList._errorList.filter(searchMainError);

        // se esistono messaggi differenziati nello stesso httpCode ne cerco uno
        if (mainErrFind.length > 0) {
            // ho trovato il codice http principale
            let myobj = mainErrFind[0];
            retval.message = mainErrFind[0].defaultMessage;
            if (myobj['notToRemoteLog']) {
                retval.notToRemoteLog = true;
            }
            if (myobj['hideFromInterface']) {
                retval.hideFromInterface = true;
            }

            if (myobj.messageList.length > 0) {
                // ho almeno un messaggio ad hoc
                let errList: any = mainErrFind[0].messageList;
                let codeErrFind = errList.filter(searchCodeError);
                if (codeErrFind.length > 0) {
                    retval.message = codeErrFind[0][1];
                }
            }
        }

        // restituisco il messaggio di risposta
        return retval;


        function searchMainError(element, index, array){
            return element.httpCode == objError.status;
        }

        function searchCodeError(element, index, array) {
            return element[0] == objError.httpCode;
        }
    }

    /**
     * elenco di tutti gli errori provenienti da laravel
     *
     * httpCode: codice http dell'errore
     * defaultMessage: messaggio di default mostrato
     * messageList: lista dei messaggi custom nel caso in cui ce ne sia più d'uno all'interno dello stesso http code
     *              ['messaggio di errore da laravel','messaggio da mostrare']
     */
    private static _errorList = [
        {
            httpCode: 0,
            defaultMessage: "Servizio non raggiungibile. Verificate la vostra connessione internet e riprovate.",
            notToRemoteLog: false,
            hideFromInterface: false,
            messageList: []
        },
        {
            httpCode: 401,
            defaultMessage: "Non autenticato",
            notToRemoteLog: false,
            hideFromInterface: true,
            messageList: []
        },
        {
            httpCode: 404,
            defaultMessage: "Risorsa non trovata",
            notToRemoteLog: false,
            hideFromInterface: false,
            messageList: [
                ["Not Found", "Operazione non effettuata. Il dato non è presente a database"]
            ]
        },
        {
            httpCode: 403,
            defaultMessage: "Non autorizzato",
            notToRemoteLog: false,
            hideFromInterface: true,
            messageList: []
        },
        {
            httpCode: 422,
            defaultMessage: "Non è stato possibile processare la richiesta",
            messageList: [
            ]
        },
        {
            httpCode: 500,
            defaultMessage: "Errore server",
            notToRemoteLog: false,
            hideFromInterface: false,
            messageList: []
        }
    ]
}
