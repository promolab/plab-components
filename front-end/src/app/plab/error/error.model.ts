export class PlabErrorModel {
    public message: string = "Si è verificato un errore imprevisto.";
    public icon: string = "";
    public notToRemoteLog: boolean = false;
    public hideFromInterface: boolean = false;
}