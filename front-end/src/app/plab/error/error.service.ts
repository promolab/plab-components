import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { UUID } from 'angular2-uuid';
import { ErrorList } from './error.list';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class ErrorService {
  constructor(private snackBar: MatSnackBar) {}

  public checkError = error => {
    console.error('error', error);

    let returnError = ErrorList.GetErrorMessage(error);

    console.error('returnError', returnError);

    if (!returnError.notToRemoteLog) {
      this.sendToLogServer(error);
    }

    if (!returnError.hideFromInterface) {
      this.snackBar.open(returnError.message, 'Ok', {
        panelClass: ['snackbar-error']
      });
    }

    return returnError;
  };

  private sendToLogServer = error => {
    console.error(error);
  };
}
