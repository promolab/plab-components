import { FormControl } from '@angular/forms';

export class MetaModel {
  public isSelected: boolean;
  public isNew: boolean;
  public isDeleting: boolean;
  public isSaving: boolean;
  public originalState: any;
  public formControl: FormControl;

  constructor(obj?) {
    if (obj) {
      this.isSelected = obj.isSelected;
      this.isNew = obj.isNew;
      this.isDeleting = obj.isDeleting;
      this.isSaving = obj.isSaving;
      this.originalState = obj.originalState;
    } else {
      this.isSelected = false;
      this.isNew = false;
      this.isDeleting = false;
      this.isSaving = false;
      this.formControl = null; // Di default non lo istanzio
    }
  }
}
