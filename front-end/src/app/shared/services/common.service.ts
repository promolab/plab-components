import { Injectable } from '@angular/core';

@Injectable()
export class CommonService {
  constructor() {}

  get getEmailPattern() {
    return "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
  }

  public getArrayYears = how => {
    let d = new Date();
    let year = d.getFullYear();
    let years: Array<number> = [];

    for (let i = 0; i < 50; i++) {
      if (how == 'backwards') {
        year = year - 1;
      }

      if (how == 'forwards') {
        year = year + 1;
      }

      years.push(year);
    }

    return years;
  };
}
