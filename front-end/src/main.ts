import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { AppModule } from './app/app.module';

if (process.env.ENV === 'production') {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);

/**
 * Sovrascrivo prototype per evitare che angular quando invia date tolga dalla data l'offset per la timezone
 * facendo diventare tutte le date con ora 00 e GMT+1 con il giorno precedente e ora 23:00
 */
let plabOriginalToISOString = Date.prototype.toISOString;
Date.prototype.toISOString = function(){
  let tzoffset = (this).getTimezoneOffset() * 60000;
  let newdate = new Date(this - tzoffset);
  let localISOTime = plabOriginalToISOString.apply(newdate);
  return localISOTime;
};
