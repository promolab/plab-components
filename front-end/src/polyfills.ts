import 'ts-helpers';
import 'core-js/proposals/reflect-metadata';
// import 'core-js/es/promise';
require('zone.js/dist/zone');


if (process.env.ENV === 'production') {
  // production
} else {
  // development
  Error['stackTraceLimit'] = Infinity;
  require('zone.js/dist/long-stack-trace-zone');
}
