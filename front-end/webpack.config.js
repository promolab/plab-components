// Helper: root(), and rootDir() are defined at the bottom
const path = require('path');
const webpack = require('webpack');
const colors = require('chalk');

// Webpack Plugins
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const {
  BaseHrefWebpackPlugin
} = require('base-href-webpack-plugin');
const {
  AngularCompilerPlugin
} = require('@ngtools/webpack');

const log = console.log;

// Env
// Get npm lifecycle event to identify the environment
const env = process.env.npm_lifecycle_event == 'build' ? 'prod' : 'dev';
const env_config = require('./config/env.' + env + '.js')(root);
const isTest = env == 'dev';
const isProd = env == 'prod';
const prefix = [colors.gray('[Config]'), colors.bold.green(':')];

log(...prefix, colors ` {yellow.bold Environment} : {${isProd ? 'red.bold' : 'green'} ${isProd ? 'production' : 'development'}}`);
log(...prefix, colors ` {${isTest ? 'green.bold' : 'yellow.bold'} isTest}      : {${isTest ? 'green' : 'red'} ${isTest}}`);
log(...prefix, colors ` {${isTest ? 'green.bold' : 'yellow.bold'} isProd}      : {${isProd ? 'green' : 'red'} ${isProd}}`);
log(...prefix, colors ` {${isTest ? 'green.bold' : 'yellow.bold'} Devtool}     : ${ env_config.devtool }`);
log(...prefix, colors ` {${isTest ? 'green.bold' : 'yellow.bold'} Base href}   : ${ isProd || env_config.BASE_HREF ? env_config.BASE_HREF : '/' }`);
log(...prefix, colors ` {${isTest ? 'green.bold' : 'yellow.bold'} Api url}     : ${ env_config.process.env.API_URL.replace(/"/g,'') }`);
log(...prefix, colors ` {${isTest ? 'green.bold' : 'yellow.bold'} File loaded} : ./config/env.${env}.js`);
log('\n');


const postCssOptions = {
  parser: 'postcss-scss',
  plugins: () => [
    autoprefixer({
      browsers: ['last 3 versions', 'iOS 9', 'ie >= 11']
    })
  ]
};

const baseScssLoaders = [{
    loader: 'postcss-loader',
    options: postCssOptions
  },
  {
    loader: 'sass-loader',
    options: {
      sourceMap: isProd
      // uncomment to have $env sass variable in scss files as 'prod'|'dev'
      // data: `$env: ${env};`
    }
  }
];

module.exports = (function makeWebpackConfig() {
  // Config
  // This is the object where all configuration gets set
  var config = {};

  // Devtool
  // Type of sourcemap to use per build type
  config.devtool = env_config.devtool;

  // add debug messages
  config.mode = isProd ? 'production' : 'development';

  // Entry
  config.entry = {
    polyfills: ['./src/polyfills.ts'],
    vendor: [
      './src/vendor.ts',
      './src/styles.ts',
      '@angular/core',
      '@angular/platform-browser',
      '@angular/platform-browser-dynamic',
      '@angular/forms',
      '@angular/material',
      '@angular/common',
      '@angular/http',
      'ngx-webstorage',
      'ng2-file-upload',
      'ngx-virtual-scroller',
      'angular-l10n',
    ],
    app: ['./src/main.ts']
  };

  // Output
  config.output = {
    path: root('dist'),
    publicPath: isProd || env_config.BASE_HREF ? env_config.BASE_HREF : '/',
    filename: 'static/js/[name].[hash].js',
    chunkFilename: 'static/js/[id].chunk.[hash].js',
    sourceMapFilename: '[file].map'
  };

  config.performance = {
    hints: false,
  };

  // Resolve
  config.resolve = {
    // only discover files that have these extensions
    extensions: ['.ts', '.js', '.json', '.css', '.scss', '.html'],

    alias: {
      variables: path.join(__dirname, 'src/static/css/_variables.scss'),
      images: path.join(__dirname, 'src/static/images'),
      icons: path.join(__dirname, 'src/static/images/icons'),
    }
  };

  config.optimization = {
    splitChunks: {
      name: true,
      chunks: 'all',
    }
  };

  // Loaders
  // This handles most of the magic responsible for converting modules
  config.module = {
    rules: [
      // Support for .ts files.
      {
        test: /\.ts$/,
        use: isProd ? ['@ngtools/webpack'] : ['ts-loader', 'angular2-template-loader', 'angular2-router-loader'],
        exclude: [/\.(spec|e2e)\.ts$/, /node_modules\/(?!(ng2-.+))/]
      },

      // Mark files inside `@angular/core` as using SystemJS style dynamic imports.
      // Removing this will cause deprecation warnings to appear.
      {
        test: /[\/\\]@angular[\/\\]core[\/\\].+\.js$/,
        parser: {
          system: true
        },
      },

      // Images
      {
        test: /\.(jpe?g|png|gif|ico)$/,
        use: [{
          loader: 'url-loader',
          options: {
            name: (isProd || env_config.BASE_HREF ? env_config.BASE_HREF : '') + 'static/images/[name].[ext]'
          }
        }]
      },

      // Fonts & SVGs
      {
        test: /\.((svg|woff2?|ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9]))|(svg|woff2?|ttf|eot)$/,
        use: 'file-loader?name=static/fonts/[name].[ext]'
      },

      // Support for *.json files.
      {
        test: /\.json$/,
        use: ['json-loader']
      },

      // Support for external .scss files
      // All scss files outside of src/app/
      {
        test: /\.scss$/,
        exclude: root('src', 'app'),
        use: [
          isProd ? MiniCssExtractPlugin.loader : 'style-loader',
          'css-loader',
          ...baseScssLoaders
        ]
      },

      // Support for external CSS files
      // All css files outside of src/app/
      {
        test: /\.css$/,
        exclude: root('src', 'app'),
        use: [
          isProd ? MiniCssExtractPlugin.loader : 'style-loader',
          'css-loader'
        ]
      },

      // Style loaders for component-declared scss styles (via styleUrls: [''])
      {
        test: /\.scss$/,
        include: root('src', 'app'),
        use: [...baseScssLoaders]
      },

      // support for .html as raw text
      {
        test: /\.html$/,
        use: 'html-loader'
      }
    ]
  };

  // Plugins
  config.plugins = [
    // Inject script and link tags into html files
    new HtmlWebpackPlugin({
      chunksSortMode: 'auto',
      inject: 'body',
      template: 'index.html'
    }),

    // Define base href in index.html
    new BaseHrefWebpackPlugin({
      baseHref: isProd || env_config.BASE_HREF ? env_config.BASE_HREF : '/'
    }),

    new ScriptExtHtmlWebpackPlugin({
      defaultAttribute: 'async'
    }),

    // Extract css files
    new MiniCssExtractPlugin({
      filename: isTest ? '[name].css' : 'static/css/[name].[hash].css',
      chunkFilename: isTest ? '[id].css' : 'static/css/[id].[hash].css'
    }),

    // Copy assets from the public folder
    new CopyWebpackPlugin([{
      from: root('src', 'static', 'images'),
      to: './static/images/'
    }]),

    // Define env variables to help with builds
    new webpack.DefinePlugin({
      // Environment helpers
      'process.env': env_config.process.env,
      ENV: env_config.process.env
    }),

    new webpack.ContextReplacementPlugin(
      // The (\\|\/) piece accounts for path separators in *nix and Windows
      /angular(\\|\/)core/,
      root('src') // location of your src
    )
  ];

  if (isTest) {
    // Dev server configuration
    config.devServer = {
      contentBase: root('src'),
      historyApiFallback: true,
      open: false,
      progress: true,
      https: false,
      stats: 'minimal', // none (or false), errors-only, minimal, normal (or true) and verbose
      host: env_config.DEV_SERVER_HOST,
      port: env_config.DEV_SERVER_PORT,
      clientLogLevel: 'error'
    };
  }

  if (isProd) {
    config.optimization.minimize = false;
    // add production specific plugins
    config.plugins.push(
      new AngularCompilerPlugin({
        tsConfigPath: './tsconfig.json',
        entryModule: 'src/app/app.module#AppModule'
      }),
      new TerserPlugin({
        parallel: true,
        sourceMap: true,
        terserOptions: {
          ecma: 6,
          keep_classnames: false,
          keep_fnames: false,
          mangle: {
            keep_classnames: false,
            keep_fnames: false,
            toplevel: true
          }
        }
      })
    );
  }

  return config;
})();

// Helper functions
function root(args) {
  args = Array.prototype.slice.call(arguments, 0);
  return path.join.apply(path, [__dirname].concat(args));
}