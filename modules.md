# Moduli

Questo file elenca tutti i moduli, ha lo scopo di tenere traccia di tutti i moduli presenti in questa repo. <br>
Per modulo si intende tutte quelle situazioni standard che devono essere gestite che richiedono l'integrazione di front-end con back-end, in sintesi il modulo ha una struttura più articolata del componente.

## Uploads

### Front-end

Componente front-end che permette di inviare i files al server

### Back-end

Classi e scheletro database per effettuare l'upload del file
